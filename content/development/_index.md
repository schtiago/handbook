---
title: Development
weight: 1
---

# Development Team

The software developers.


# Getting Started

## DS Dept. Templates

### Cookiecutter: how to use

`pip install cookiecutter`

`conda install -c conda-forge cookiecutter`

`cookiecutter https://github.com/drivendata/cookiecutter-data-science`

### Usefull links

https://towardsdatascience.com/automate-the-structure-of-your-data-science-projects-with-cookiecutter-937b244114d8

https://drivendata.github.io/cookiecutter-data-science/
